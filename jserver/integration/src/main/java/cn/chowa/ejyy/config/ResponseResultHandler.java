package cn.chowa.ejyy.config;

import cn.chowa.ejyy.common.CodeException;
import cn.dev33.satoken.util.SaResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Map;

@ControllerAdvice
public class ResponseResultHandler implements ResponseBodyAdvice<Object> {
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType,
                                  MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request, ServerHttpResponse response) {
        if (body instanceof SaResult) {
            SaResult result = (SaResult) body;
            return new ApiResponse(result.getCode(), result.getMsg(), result.getData(), System.currentTimeMillis());
        } else if (body instanceof GlobalExceptionHandler.RequestResult) {
            return body;
        } else if (body instanceof Map) {
            Map map = (Map) body;
            //spring mvc内部异常
            if (map.containsKey("timestamp") && map.containsKey("status") && map.containsKey("error")) {
                return new ApiResponse((Integer) map.get("status"), (String) map.get("error"),
                        "", System.currentTimeMillis());
            }
        }

        return new ApiResponse(200, "", body, System.currentTimeMillis());
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ApiResponse {
        private int code;
        private String message;
        private Object data;
        private long timestamp;
    }
}
