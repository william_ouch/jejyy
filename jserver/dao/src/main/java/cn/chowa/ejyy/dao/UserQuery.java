package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

@JqlQuery
public interface UserQuery {

    ObjData getUserInfo(int leaveOffice, String account);

}
