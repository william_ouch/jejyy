package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.PropertyCompanyBuildingRegistered;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyCompanyBuildingRegisteredRepository extends JpaRepository<PropertyCompanyBuildingRegistered, Long> {

    PropertyCompanyBuildingRegistered findByBuildingId(long buildingId);

}
