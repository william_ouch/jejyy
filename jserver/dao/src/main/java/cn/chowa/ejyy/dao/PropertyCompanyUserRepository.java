package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.OwerApply;
import cn.chowa.ejyy.model.entity.PropertyCompanyUser;
import cn.chowa.ejyy.model.entity.PropertyCompanyUserLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

/**
 * @ClassName PropertyCompanyUserRepository
 * @Description TODO
 * @Author ironman
 * @Date 16:51 2022/8/17
 */
public interface PropertyCompanyUserRepository extends JpaRepository<PropertyCompanyUser, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update ejyy_property_company_user set password=?2 where id=?1", nativeQuery = true)
    void saveOne(long id, String password);

}
