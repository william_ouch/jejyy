package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.RepairUrge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepairUrgeRepository extends JpaRepository<RepairUrge, Long> {

    long countByRepairId(long repairId);

}
