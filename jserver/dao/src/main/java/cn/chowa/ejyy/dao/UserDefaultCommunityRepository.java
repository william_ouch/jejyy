package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.UserDefaultCommunity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDefaultCommunityRepository extends JpaRepository<UserDefaultCommunity, Long> {

    UserDefaultCommunity findByWechatMpUserId(long wechatMpUserId);

}
