package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.PropertyCompanyUserAccessCommunity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyCompanyUserAccessCommunityRepository extends JpaRepository<PropertyCompanyUserAccessCommunity, Long> {

    PropertyCompanyUserAccessCommunity findByPropertyCompanyUserIdAndCommunityId(long propertyCompanyUserId, long communityId);

}
