package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.WechatMpUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WechatMpUserRepository extends JpaRepository<WechatMpUser, Long> {

    WechatMpUser findByPhoneAndIntact(String phone, int intact);

}
