package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.Complain;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ComplainRepository extends JpaRepository<Complain, Long> {

    List<Complain> findByCommunityIdAndCreatedAtBetween(long communityId, long start, long end);

}
