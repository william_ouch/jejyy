function getCommunityNotices(community_id,published, page_size, page_num){
    var where="1=1 and ";
    where+=published>=0?"a.published=:published and ":"";

    var sql=`
        select
        a.id,
        a.title,
        a.published,
        a.published_at,
        a.notice_tpl_id,
        a.created_by,
        a.created_at,
        b.real_name
        from ejyy_notice_to_user a left join
        ejyy_property_company_user b on a.created_by=b.id
        where ${where} a.community_id=:community_id
        limit ${(page_num-1)*page_size},${page_size}
    `;

    return sql;
}

function getCommunityNoticesCount(community_id,published){
    var where="1=1 and ";
    where+=published>=0?"a.published=:published and ":"";

    var sql=`
        select
        count(*)
        from ejyy_notice_to_user a left join
        ejyy_property_company_user b on a.created_by=b.id
        where ${where} a.community_id=:community_id
    `;

    return sql;
}

function getCommunityNotice(){
    var sql=`
        select
            a.id,
            a.title,
            a.overview,
            a.content,
            a.created_at,
            a.notice_tpl_id,
            a.published,
            a.published_at,
            a.created_by,
            a.published_by,
            b.tpl,
            b.content as tpl_content,
            c.real_name
        from ejyy_notice_to_user a left join
            ejyy_notice_tpl b on a.notice_tpl_id=b.id left join
            ejyy_property_company_user c on b.id = a.created_by
        where a.id =:id and a.community_id=:community_id
    `;
    return sql;
}

