package cn.chowa.ejyy.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_user_car_operate_log")
public class UserCarOperateLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long user_car_id;

    private Long wechat_mp_user_id;

    private Long property_company_user_id;

    /**
     * 1 解绑；0 绑定
     */
    private int status;

    /**
     * 1 用户 2家人 3物业公司
     */
    private int operate_by;

    private long created_at;

}
