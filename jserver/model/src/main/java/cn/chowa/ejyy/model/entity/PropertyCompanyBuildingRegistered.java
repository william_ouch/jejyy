package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "ejyy_property_company_building_registered")
public class PropertyCompanyBuildingRegistered {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("building_id")
    private long buildingId;

    @NotNull(message = "名称不能为空")
    @Size(max = 12, message = "名称长度不能超过12")
    private String name;

    private int gender;

    @NotNull(message = "身份证不能为空")
    @Size(max = 18, message = "身份证长度不能超过12")
    private String idcard;

    @NotNull(message = "手机号不能为空")
    @Size(max = 11, message = "手机号码长度不能超过11")
    private String phone;

    @JsonProperty("created_by")
    private Long createdBy;

    @JsonProperty("created_at")
    private long createdAt;

}
