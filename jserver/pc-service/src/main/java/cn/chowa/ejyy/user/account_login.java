package cn.chowa.ejyy.user;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.dao.PropertyCompanyAuthRepository;
import cn.chowa.ejyy.dao.PropertyCompanyUserLoginRepository;
import cn.chowa.ejyy.dao.UserQuery;
import cn.chowa.ejyy.model.entity.PropertyCompanyAuth;
import cn.chowa.ejyy.model.entity.PropertyCompanyUserLogin;
import cn.chowa.ejyy.service.property_company;
import cn.chowa.ejyy.vo.PostInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.crypto.SecureUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/pc/user")
public class account_login {

    @Autowired
    private UserQuery userQuery;
    @Autowired
    private PropertyCompanyAuthRepository companyAuthRepository;
    @Autowired
    private PropertyCompanyUserLoginRepository userLoginRepository;
    @Autowired
    private property_company propertycompany;

    /**
     * 账号登录
     */
    @PostMapping("/account_login")
    public Map<String, Object> login(@RequestBody RequestData param,
                                     HttpSession session,
                                     HttpServletRequest request) {
        AccountLogin login = param.getData(AccountLogin.class);

        LineCaptcha captcha = (LineCaptcha) session.getAttribute("captcha");
        if (!captcha.verify(login.getCaptcha())) {
            throw new CodeException(Constants.code.CAPTCHA_ERROR, "验证码错误");
        }

        ObjData userInfo = userQuery.getUserInfo(Constants.status.FALSE, login.getAccount());
        if (userInfo == null || !userInfo.getStr("password")
                .equals(SecureUtil.md5(login.getPassword()))) {
            throw new CodeException(Constants.code.PWD_ERROR, "密码错误或账号不存在");
        }

        //登录并获取token
        StpUtil.login(login.getAccount(), "PC");
        String token = StpUtil.getTokenInfo().getTokenValue();

        //隐藏电话
        String phone = userInfo.getStr("phone");
        if (phone != null) {
            phone = phone.substring(0, 3) + "****" + phone.substring(7, 11);
            userInfo.put("phone", phone);
        }
        userInfo.set("access", userInfo.getStr("content"));
        long uid = userInfo.getLong("id");

        //保存token
        PropertyCompanyAuth companyAuth = companyAuthRepository.findById(uid).get();
        companyAuth.setToken(token);
        companyAuthRepository.save(companyAuth);

        //添加用户登录记录
        userLoginRepository.save(new PropertyCompanyUserLogin(
                0,
                uid,
                request.getRemoteAddr(),
                request.getHeader("User-Agent"),
                System.currentTimeMillis()
        ));

        PostInfo postInfo = propertycompany.getPostInfo(uid);

        LoginUserInfo loginUserInfo = new LoginUserInfo();
        BeanUtil.copyProperties(userInfo, loginUserInfo, true);

        return Map.of("token", token,
                "userInfo", loginUserInfo,
                "postInfo", postInfo);
    }

    @Data
    static class AccountLogin {
        private String account;
        private String password;
        private String captcha;
    }


    @Data
    static class LoginUserInfo {

        private long id;

        private String account;

        @JsonProperty("real_name")
        private String realName;

        private int gender;

        @JsonProperty("avatar_url")
        private String avatarUrl;

        private String phone;

        @JsonProperty("join_company_at")
        private long joinCompanyAt;

        private int admin;

        @JsonProperty("created_at")
        private long createdAt;

        private int subscribed;

        private String access;

    }

}
