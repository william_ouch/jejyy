package cn.chowa.ejyy.notice;

import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.NoticeToUserRepository;
import cn.chowa.ejyy.dao.NoticeTplRepository;
import cn.chowa.ejyy.dao.UserQuery;
import cn.chowa.ejyy.model.entity.NoticeToUser;
import cn.chowa.ejyy.model.entity.NoticeTpl;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController("noticeCreate")
@RequestMapping("/pc/notice")
public class create {

    @Autowired
    private UserQuery userQuery;

    @Autowired
    private NoticeToUserRepository noticeToUserRepository;

    @Autowired
    private NoticeTplRepository noticeTplRepository;

    /**
     * 小区通知 - 新增
     */
    @SaCheckRole(Constants.RoleName.XQTZ)
    @VerifyCommunity(true)
    @PostMapping("/create")
    public Map<?, ?> create(@RequestBody RequestData data) {
        String title = data.getStr("title", true, "");
        String overview = data.getStr("overview", true, "");
        String content = data.getJsonStr("content", true);
        boolean published = data.getBool("published", true, "^0|1$");
        boolean oa_tpl_msg = data.getBool("oa_tpl_msg", true);
        String tpl = data.getStr("tpl");
        String tpl_content = data.getJsonStr("tpl_content");

        /*
         {
         "title": "AAA",
         "overview": "aaaa",
         "published": 1,
         "oa_tpl_msg": false,
         "tpl": "",
         "tpl_content": [],
         "content": [{
         "tag": "p",
         "attrs": [{
         "name": "data-we-empty-p",
         "value": ""
         }],
         "children": ["aaaaaaa"]
         }],
         "community_id": 1
         }
         */

        long created_at = System.currentTimeMillis();
        long notice_tpl_id = 0;
        if (oa_tpl_msg) {
            notice_tpl_id = noticeTplRepository.save(new NoticeTpl(0, tpl, tpl_content)).getId();
        }

        long id = noticeToUserRepository.save(
                NoticeToUser.builder()
                        .title(title)
                        .overview(overview)
                        .communityId(data.getCommunityId())
                        .createdBy(AuthUtil.getUid())
                        .content(content)
                        .published(published)
                        .publishedAt(published ? created_at : null)
                        .publishedBy(published ? AuthUtil.getUid() : null)
                        .noticeTplId(notice_tpl_id)
                        .createdAt(created_at)
                        .build()).getId();

        //{overview=测试概述, tpl=, tpl_content=[], community_id=1, oa_tpl_msg=false, published=1, title=AAAAA, content=[{tag=p, attrs=[{name=data-we-empty-p, value=}], children=[通知内人1111]}]}
        //效验，如果发布，则单独执行通知操作
        if (published) {
            // TODO: 2022/8/18  noticeService.broadcast(ctx.model, id);
        }

        return Map.of("id", id);
    }

}
