package cn.chowa.ejyy.building;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.BuildingInfoRepository;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.chowa.ejyy.dao.PropertyCompanyBuildingRegisteredRepository;
import cn.chowa.ejyy.dao.UserCarRepository;
import cn.chowa.ejyy.model.entity.BuildingInfo;
import cn.chowa.ejyy.model.entity.PropertyCompanyBuildingRegistered;
import cn.chowa.ejyy.model.entity.UserCar;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/pc/building")
public class detail {

    @Autowired
    private BuildingInfoRepository buildingInfoRepository;
    @Autowired
    private PropertyCompanyBuildingRegisteredRepository propertyCompanyBuildingRegisteredRepository;
    @Autowired
    private BuildingQuery buildingQuery;
    @Autowired
    private UserCarRepository userCarRepository;

    /**
     * 房产详情
     */
    @SaCheckRole(Constants.RoleName.FCDA)
    @VerifyCommunity(true)
    @PostMapping("/detail")
    public Map<?, ?> getDetail(@RequestBody RequestData data) {
        int id = (int) data.getId();

        Optional<BuildingInfo> optBuildingInfo = buildingInfoRepository.findById(id);
        if (optBuildingInfo.isEmpty()) {
            throw new CodeException(Constants.code.QUERY_ILLEFAL, "非法的固定资产查询");
        }
        BuildingInfo info = optBuildingInfo.get();

        PropertyCompanyBuildingRegistered registered = propertyCompanyBuildingRegisteredRepository.findByBuildingId(id);

        List<ObjData> owners = new ArrayList<>();

        if (AuthUtil.hasAnyRole(Constants.Role.YZDA)) {
            owners = buildingQuery.getBuildingOwner(id);
        }

        List<UserCar> userCars = new ArrayList<>();
        int type = info.getType();

        if (AuthUtil.hasAllRole(Constants.Role.CLGL) &&
                (type == Constants.building.CARPORT || type == Constants.building.GARAGE)) {
            userCars = userCarRepository.findByBuildingId(id);
        }

        return Map.of(
                "info", info,
                "registered", registered,
                "owers", owners,
                "cars", userCars
        );
    }
}
