package cn.chowa.ejyy.common.utils;

import cn.chowa.ejyy.common.Constants;
import cn.dev33.satoken.stp.StpUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AuthUtil {

    public static long getUid() {
        return 1;
    }

    public static String getUserId() {
        return StpUtil.getLoginId().toString();
    }

    public static List<String> getUserRoles() {
        return StpUtil.getRoleList();
    }

    public static boolean hasAllRole(Constants.Role... roles) {
        return getUserRoles().containsAll(Arrays.asList(roles));
    }

    public static boolean hasAnyRole(Constants.Role... roles) {
        List<String> userRoles = getUserRoles();
        for (Constants.Role r : roles) {
            if (userRoles.contains(r)) {
                return true;
            }
        }
        return false;
    }

}
