package cn.chowa.ejyy.user;


import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.utils.PhoneUtil;
import cn.chowa.ejyy.dao.WechatMpAuthRepository;
import cn.chowa.ejyy.dao.WechatMpUserLoginRepository;
import cn.chowa.ejyy.dao.WechatMpUserQuery;
import cn.chowa.ejyy.dao.WechatMpUserRepository;
import cn.chowa.ejyy.model.entity.WechatMpAuth;
import cn.chowa.ejyy.model.entity.WechatMpUser;
import cn.chowa.ejyy.model.entity.WechatMpUserLogin;
import cn.chowa.ejyy.service.CommunityService;
import cn.hutool.crypto.SecureUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static cn.chowa.ejyy.common.Constants.status.INCOMPLETE_USER_INFO;

@Slf4j
@RestController
@RequestMapping("/mp/user")
public class login {

    @Autowired
    private WechatMpUserQuery wechatMpUserQuery;
    @Autowired
    private WechatMpUserRepository wechatMpUserRepository;
    @Autowired
    private WechatMpAuthRepository wechatMpAuthRepository;
    @Autowired
    private WechatMpUserLoginRepository wechatMpUserLoginRepository;
    @Autowired
    private CommunityService communityService;

    @PostMapping("/login")
    public Map<String, Object> login(@RequestBody RequestData data, HttpServletRequest request) {
        String code = data.getStr("code", true, "^[0-9a-zA-Z-_\\$]{32}$");
        String brand = data.getStr("brand", true, null);
        String model = data.getStr("model", true, null);
        String system = data.getStr("system", true, null);
        String platform = data.getStr("platform", true, null);

        //todo 调用微信登录接口 https://api.weixin.qq.com/sns/jscode2session
        ObjData mpSessionInfo = new ObjData(Map.of(
                "openid", "openid1234567",
                "unionid", "unionid1234567"
        ));
        String openid = mpSessionInfo.getStr("openid");

        String token = SecureUtil.md5(openid + System.currentTimeMillis());
        ObjData mpUserInfo = wechatMpUserQuery.getWechatMpUserInfo(openid);
        if (mpUserInfo == null) {
            mpUserInfo = new ObjData();
            mpUserInfo.set("open_id", openid);
            mpUserInfo.set("union_id", mpSessionInfo.getStr("unionid"));
            mpUserInfo.set("phone", null);
            mpUserInfo.set("created_at", System.currentTimeMillis());

            mpUserInfo.set("gender", 1);
            mpUserInfo.set("intact", INCOMPLETE_USER_INFO);
            mpUserInfo.set("signature", "不一定每天都很好，但每天都会有些小美好在等你");
            mpUserInfo.set("subscribed", false);

            WechatMpUser wmu = wechatMpUserRepository.save(
                    WechatMpUser.builder()
                            .openId(openid)
                            .unionId(mpSessionInfo.getStr("unionid"))
                            .createdAt(System.currentTimeMillis())
                            .gender(1)
                            .intact(INCOMPLETE_USER_INFO)
                            .signature(mpUserInfo.getStr("signature"))
                            .build());
            mpUserInfo.set("id", wmu.getId());

            mpUserInfo.remove("open_id");
            mpUserInfo.remove("union_id");

            wechatMpAuthRepository.save(WechatMpAuth.builder()
                    .wechatMpUserId(wmu.getId())
                    .token(token)
                    .build());
        } else {
            mpUserInfo.set("phone", PhoneUtil.hide(mpUserInfo.getStr("phone")));
            WechatMpAuth wma = wechatMpAuthRepository.findByWechatMpUserId(mpUserInfo.getLong("id"));
            wma.setToken(token);
            wechatMpAuthRepository.save(wma);
        }

        wechatMpUserLoginRepository.save(WechatMpUserLogin.builder()
                .wechat_mp_user_id(mpUserInfo.getLong("id"))
                .ip(request.getRemoteHost())
                .brand(brand)
                .model(model)
                .system(system)
                .platform(platform)
                .login_at(System.currentTimeMillis())
                .build());

        ObjData communityInfo = communityService.communityService(mpUserInfo.getLong("id"));

        return Map.of(
                "token", token,
                "userInfo", mpUserInfo,
                "communityInfo", communityInfo
        );
    }

}
